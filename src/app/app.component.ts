import { Component } from '@angular/core';

import { MenuComponent } from '../features/menu';
import { SidebarComponent } from '../features/sidebar';
import { LanguageComponent } from '../features/i18n/language.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    LanguageComponent,
    MenuComponent,
    SidebarComponent
  ],
  template: `
    <app-language />
    <app-menu />
    <app-sidebar />
  `
})
export class AppComponent {
  title = 'i18n';
}
