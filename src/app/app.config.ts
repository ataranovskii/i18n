import { APP_INITIALIZER, ApplicationConfig, importProvidersFrom, LOCALE_ID } from '@angular/core';
import { provideRouter } from '@angular/router';

import { I18NEXT_SERVICE, I18NextModule, ITranslationService } from 'angular-i18next';
import * as i18n from 'i18next';

import { routes } from './app.routes';

const i18nOptions: i18n.InitOptions = {
  supportedLngs: [ 'en', 'ru' ],
  fallbackLng: 'en',
  debug: true,
  returnEmptyString: false,
  ns: [
    'translation',
    'validation',
    'error'
  ],
  resources: {
    en: {
      translation: {
        common_hello: 'Hello'
      }
    },
    ru: {
      translation: {
        common_hello: 'Привет'
      }
    }
  }
}

export function appInit(i18next: ITranslationService) {
  return () => i18next.init(i18nOptions);
}

export function localeIdFactory(i18next: ITranslationService)  {
  return i18next.language;
}

export const I18N_PROVIDERS = [
  {
    provide: APP_INITIALIZER,
    useFactory: appInit,
    deps: [I18NEXT_SERVICE],
    multi: true
  },
  {
    provide: LOCALE_ID,
    deps: [I18NEXT_SERVICE],
    useFactory: localeIdFactory
  }];

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(I18NextModule.forRoot()),
    provideRouter(routes),
    I18N_PROVIDERS
  ]
};
