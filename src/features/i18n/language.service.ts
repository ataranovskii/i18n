import { Injectable } from '@angular/core';
import { Language } from './language.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  defaultLanguage = Language.EN;
  currentLanguageSubj = new BehaviorSubject<Language>(this.defaultLanguage);
  currentLanguage$ = this.currentLanguageSubj.asObservable();

  changeLanguage(language: Language) {
    this.currentLanguageSubj.next(language);
  }

}
