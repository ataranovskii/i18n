import { Component, inject } from '@angular/core';
import { Language } from './language.model';
import { AsyncPipe, NgForOf } from '@angular/common';
import { LanguageService } from './language.service';

@Component({
  standalone: true,
  selector: 'app-language',
  imports: [ NgForOf, AsyncPipe ],
  template: `
    <button
      *ngFor="let language of languages"
      (click)="languageService.changeLanguage(language)"
      [class.active]="(languageService.currentLanguage$ | async) === language"
    >{{ language }}</button>
    <span>Selected: {{ languageService.currentLanguage$ | async }}</span>
  `,
  styles: `
    :host {
      display: flex;
      align-items: center;
      gap: 10px;
    }
  `
})
export class LanguageComponent {

  public languageService = inject(LanguageService);

  languages: Language[] = [
    Language.EN,
    Language.RU,
  ];

}
